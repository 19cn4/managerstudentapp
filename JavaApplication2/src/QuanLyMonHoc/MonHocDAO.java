/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package QuanLyMonHoc;

import Login.ConnectDB;
import MenuChinh.MenuChinh;
import static QuanLyMonHoc.MonHoc.conn;
import static QuanLyMonHoc.MonHoc.loadIdMh;
import com.mysql.cj.jdbc.result.ResultSetMetaData;
import java.awt.Color;
import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Stack;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/**
 *
 * @author AORUS
 */
public class MonHocDAO extends javax.swing.JFrame {

    Connection conn;
    public PreparedStatement pstmt;
    public static ResultSet rs = null;
    public Statement stmt;
    private int id = -1;
    public String mMh;
    public static List<CoSo> coso = new ArrayList<>();
    ArrayList<MonHoc> arr = new ArrayList<MonHoc>();
//    ArrayList<Modelbook> arr1 = new ArrayList<Modelbook>();
    /**
     * Creates new form MonHocDAO
     */
    public MonHocDAO() throws SQLException {
        initComponents();
        MonHoc mh = new MonHoc();
        mh.connect();
        setIdmh();
        loadIdMh();
        LoadData();
        loadComboBox();
        
//        loadDataToArrayList();
//        loadDataArrayListToTable();
  

    }
    
        private void setIdmh() {
        ConnectDB con = new ConnectDB();
          
        try {
            Connection conn = con.getConnectDb();
            String queryMmh = "select max(id_mh) from monHoc";
            Statement stsm = conn.createStatement();
            ResultSet rs = stsm.executeQuery(queryMmh);
            int mmh = 0;
            while (rs.next()) {
                mmh = Integer.parseInt(rs.getString(1));
                txtMaMH.setText("" + (mmh + 1) + "");
            } 
        } catch (Exception e) {
            txtMaMH.setText("200");
            System.out.println("set auto id loi");
        }

    }

    private void LoadData() {
        MonHoc mh = new MonHoc();

        mh.connect();
        try {
            rs = mh.GetData("monHoc");
            jTable1.removeAll();
            String[] arr = {"Mã môn học", "Tên môn học", "Điểm trung bình"};
            DefaultTableModel model = new DefaultTableModel(arr, 0);

            while (rs.next()) {
                Vector vec = new Vector();
                vec.add(rs.getString("id_mh"));
               
                vec.add(rs.getString("tenMh"));
                vec.add(rs.getString("diemTB"));
                model.addRow(vec);
            }
            jTable1.setModel(model);
//            TableColumnModel tcm = jTable1.getColumnModel();
//            tcm.removeColumn(tcm.getColumn(0));

        } catch (Exception e) {
            System.out.println("Loi load data");
        }

    }
    

    private void loadComboBox() throws SQLException {
        conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/manager_student", "nhom6", "Nhom6123456");
        String sql = "SELECT * FROM manager_student.coSo";
        try (
                 PreparedStatement stmt = conn.prepareStatement(sql);  ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                CoSo cs = new CoSo(rs.getInt("id_cs"), rs.getString("tenCS"));
                coso.add(cs);
                jComboBox1.addItem(rs.getString("tenCS"));
            }
        }
    }

    public ResultSet GetData(String jtable) throws SQLException {
        ResultSet kq = null;
        Statement stmt = conn.createStatement();
        String sql = "SELECT * FROM monHoc";
        for (CoSo cs : coso) {
            if (cs.getId_cs() == Integer.parseInt(jComboBox1.getSelectedItem().toString())) {
                sql += "     AND id_cs=      ";
            }
        }

        kq = stmt.executeQuery(sql);
        try {

            jTable1.removeAll();
            String[] arr = {"Mã môn học", "Tên môn học", "Điểm trung bình"};
            DefaultTableModel model = new DefaultTableModel(arr, 0);

            while (rs.next()) {
                Vector vec = new Vector();
                vec.add(rs.getString("id_mh"));               
                vec.add(rs.getString("tenMh"));
                vec.add(rs.getString("diemTB"));
                model.addRow(vec);
            }
            jTable1.setModel(model);
//            TableColumnModel tcm = jTable1.getColumnModel();
//            tcm.removeColumn(tcm.getColumn(0));

        } catch (Exception e) {
            System.out.println("Loi");
        }
        return kq;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        kGradientPanel1 = new keeptoo.KGradientPanel();
        jLabel1 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        txtTenMH = new javax.swing.JTextField();
        txtMaMH = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        btnDel = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jSeparator2 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        kGradientPanel1.setkEndColor(new java.awt.Color(0, 255, 204));
        kGradientPanel1.setkStartColor(new java.awt.Color(0, 204, 255));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("QUẢN LÝ MÔN HỌC");

        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/btn/left-arrow.png"))); // NOI18N
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/btn/microsoft_excel_35px.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setText("Mã môn học");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel2.setText("Tên môn học :");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel4.setText("Chọn khối kiến thức ");

        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });

        txtMaMH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMaMHActionPerformed(evt);
            }
        });

        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/btn/plus_+_35px.png"))); // NOI18N
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/btn/trash.png"))); // NOI18N
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/btn/edit-button.png"))); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "ID Môn học", "Tên môn học", "Điểm trung bình"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout kGradientPanel1Layout = new javax.swing.GroupLayout(kGradientPanel1);
        kGradientPanel1.setLayout(kGradientPanel1Layout);
        kGradientPanel1Layout.setHorizontalGroup(
            kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(kGradientPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(kGradientPanel1Layout.createSequentialGroup()
                        .addComponent(jSeparator2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1))
                    .addGroup(kGradientPanel1Layout.createSequentialGroup()
                        .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(kGradientPanel1Layout.createSequentialGroup()
                                .addComponent(btnBack)
                                .addGap(155, 155, 155)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton1))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, kGradientPanel1Layout.createSequentialGroup()
                                .addGap(41, 41, 41)
                                .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(kGradientPanel1Layout.createSequentialGroup()
                                        .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel2))
                                        .addGap(37, 37, 37)
                                        .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtTenMH, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtMaMH, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(kGradientPanel1Layout.createSequentialGroup()
                                        .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(btnAdd)
                                            .addComponent(jLabel4))
                                        .addGap(26, 26, 26)
                                        .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btnDel))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnEdit)))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 648, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        kGradientPanel1Layout.setVerticalGroup(
            kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(kGradientPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtMaMH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtTenMH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(19, 19, 19)
                .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnAdd)
                    .addComponent(btnDel)
                    .addComponent(btnEdit))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(kGradientPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(kGradientPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        int i = jTable1.getSelectedRow();
        TableModel model = jTable1.getModel();
        txtMaMH.setText(model.getValueAt(i, 0).toString());
        txtTenMH.setText(model.getValueAt(i, 2).toString());
        id = Integer.parseInt(model.getValueAt(i, 0).toString());
        String tenCS = model.getValueAt(i, 2).toString();
        jComboBox1.setSelectedItem(tenCS);
    }//GEN-LAST:event_jTable1MouseClicked

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        MenuChinh menu = new MenuChinh();
        menu.show();
        this.hide();
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        // TODO add your handling code here:
        StringBuilder sb = new StringBuilder();
        if (txtMaMH.getText().equals("")) {
            sb.append("Không được để trống");
            txtMaMH.setBackground(Color.red);
        } else {
            txtMaMH.setBackground(Color.WHITE);
        }
        if (txtTenMH.getText().equals("")) {
            sb.append("Không được để trống");
            txtTenMH.setBackground(Color.red);
        } else {
            txtTenMH.setBackground(Color.WHITE);
        }
        if (jComboBox1.getSelectedItem().equals("Tất cả")) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn khối kiến thức");
            return;
        }
        if (sb.length() > 0) {
            JOptionPane.showMessageDialog(this, sb);
            return;
        }
        try {

            MonHoc mh = new MonHoc();

            if (id != - 1) {
                if (mh.edit(id, txtTenMH.getText(), txtMaMH.getText())) {
                    JOptionPane.showMessageDialog(this, "Sửa thành công");
                    ConnectDB con = new ConnectDB();
                    int mmh = 0;
                    try {
                        String queryMmh = "select max(id_mh) from monHoc";
                Statement stsm = conn.createStatement();
                ResultSet rs = stsm.executeQuery(queryMmh);
                
                while(rs.next()) {
                    mmh = Integer.parseInt(rs.getString(1));
                }
                mmh++;
                txtMaMH.setText(""+ mmh +"");
                txtTenMH.setText("");
                    } catch (Exception e) {
                        txtMaMH.setText("200");
                        txtTenMH.setText("");
                    }
                    loadComboBox1();
                } else {
                    JOptionPane.showMessageDialog(this, "Sửa thất bại");
                }
                id = -1;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        // TODO add your handling code here:
        if (jComboBox1.getSelectedItem().equals("Tất cả")) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn khối kiến thức");
            return;
        }
        try {

            MonHoc mh = new MonHoc();

            if (id != -1) {
                if (mh.delete(id)) {
                    JOptionPane.showMessageDialog(this, "Xoá thành công");
                    ConnectDB con = new ConnectDB();
                    int mmh = 0;
                    try {
                        String queryMmh = "select max(id_mh) from monHoc";
                Statement stsm = conn.createStatement();
                ResultSet rs = stsm.executeQuery(queryMmh);
                
                while(rs.next()) {
                    mmh = Integer.parseInt(rs.getString(1));
                }
                mmh++;
                txtMaMH.setText(""+ mmh +"");
                txtTenMH.setText("");
                    } catch (Exception e) {
                        txtMaMH.setText("200");
                        txtTenMH.setText("");
                    }
                    
                } else {
                    JOptionPane.showMessageDialog(this, "Xoá thất bại");
                }
                id = -1;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        loadComboBox1();
    }//GEN-LAST:event_btnDelActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        // TODO add your handling code here:
        StringBuilder sb = new StringBuilder();
        if (txtMaMH.getText().equals("") || txtTenMH.getText().equals("")) {
            sb.append("Không được để trống");
            if (txtMaMH.getText().equals("")) {
                txtMaMH.setBackground(Color.red);
            } else {
                txtMaMH.setBackground(Color.white);
            }
            if (txtTenMH.getText().equals("")) {
                txtTenMH.setBackground(Color.red);
            } else {
                txtTenMH.setBackground(Color.white);
            }
        } else {
            txtMaMH.setBackground(Color.white);
            txtTenMH.setBackground(Color.white);
        }
        if (jComboBox1.getSelectedItem().equals("Tất cả")) {
            JOptionPane.showMessageDialog(this, "Vui lòng chọn khối kiến thức");
            return;
        }

        if (sb.length() > 0) {
            JOptionPane.showMessageDialog(this, sb);
            return;
        }
        try {
            ConnectDB con = new ConnectDB();
            int mmh = 0;
            try {
                Connection conn = con.getConnectDb();
            
            String queryMmh = "select max(id_mh) from monHoc";
                Statement stsm = conn.createStatement();
                ResultSet rs = stsm.executeQuery(queryMmh);
                
                while(rs.next()) {
                    mmh = Integer.parseInt(rs.getString(1));
                }
                
                System.out.println("ma mh max: "+ mmh);
                System.out.println("rs: "+ rs);
                
            } catch (Exception e) {
               
                System.out.println("Loi khi lay id max");
            }
            MonHoc mh = new MonHoc();
            if (mh.insert(txtTenMH.getText(), txtMaMH.getText(), jComboBox1.getSelectedItem().toString())) {
                
                
                String queryMmh = "select max(id_mh) from monHoc";
                Statement stsm = conn.createStatement();
                ResultSet rs = stsm.executeQuery(queryMmh);
                
                while(rs.next()) {
                    mmh = Integer.parseInt(rs.getString(1));
                }
              
                txtMaMH.setText(""+ mmh +"");
                txtTenMH.setText("");
                JOptionPane.showMessageDialog(this, "Thêm thành công");
                mmh++;
                txtMaMH.setText(""+ mmh +"");
                loadComboBox1();
                
                
            } else {
                
                JOptionPane.showMessageDialog(this, "Đã tồn tại mã môn học !");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        loadComboBox1();
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try {
            MonHoc mh = new MonHoc();
            mh.exportExcel(jTable1);
        } catch (Exception e) {
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtMaMHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMaMHActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMaMHActionPerformed

    private void loadComboBox1() {
       conn = ConnectDB.getConnectDb1();
        String sql = "SELECT * FROM monHoc left join coSo on coSo.id_cs = monHoc.id_cs where 1=1 ";
        try {

            Statement stmt = conn.createStatement();
            for (CoSo cs : coso) {
                if (cs.getTenCS().equals(jComboBox1.getSelectedItem()) && cs.getId_cs() != 1) {
                    sql += " and monHoc.id_cs=" + cs.getId_cs() + " ";
                }
            }
            ResultSet kq = stmt.executeQuery(sql);
            jTable1.removeAll();
            String[] arr = {"Mã môn học", "Khối kiến thức", "Tên môn học", "Điểm trung bình"};
            DefaultTableModel model = new DefaultTableModel(arr, 0);

            while (kq.next()) {
                Vector vec = new Vector();
                vec.add(kq.getString("id_mh"));        
                vec.add(kq.getString("tenCS"));
                vec.add(kq.getString("tenMh"));
                vec.add(kq.getString("diemTB"));
                
                model.addRow(vec);
            }
            jTable1.setModel(model);
//            TableColumnModel tcm = jTable1.getColumnModel();
//            tcm.removeColumn(tcm.getColumn(0));
//            tcm.removeColumn(tcm.getColumn(3));
        } catch (SQLException ex) {
            Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MonHocDAO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MonHocDAO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MonHocDAO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MonHocDAO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new MonHocDAO().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(MonHocDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable jTable1;
    private keeptoo.KGradientPanel kGradientPanel1;
    private javax.swing.JTextField txtMaMH;
    private javax.swing.JTextField txtTenMH;
    // End of variables declaration//GEN-END:variables
}
