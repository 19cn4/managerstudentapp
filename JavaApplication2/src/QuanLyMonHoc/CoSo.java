/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package QuanLyMonHoc;

/**
 *
 * @author AORUS
 */
public class CoSo {
    private int id_cs;
    private String tenCS;

    public CoSo(int id_cs, String tenCS) {
        this.id_cs = id_cs;
        this.tenCS = tenCS;
    }

    public void setId_cs(int id_cs) {
        this.id_cs = id_cs;
    }

    public void setTenCS(String tenCS) {
        this.tenCS = tenCS;
    }

    public int getId_cs() {
        return id_cs;
    }

    public String getTenCS() {
        return tenCS;
    }
    
        
   
}
