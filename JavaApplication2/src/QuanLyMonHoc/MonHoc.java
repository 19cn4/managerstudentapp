/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package QuanLyMonHoc;

import Login.ConnectDB;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/**
 *
 * @author apple
 */
public class MonHoc {
    private JTable jtable = new JTable();
    private DefaultTableModel tableModel = new DefaultTableModel();
    public static Connection conn = null;
    public static ResultSet rs = null;
    public Statement stmt = null;
    
    public void connect() {
        try {
           conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/manager_student", "nhom6", "Nhom6123456");
           Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM monHoc");
            while(rs.next()) {
                System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));
            }
           if(conn != null){
                System.out.println("ket noi thanh cong");
           }
            
        } catch (SQLException e) {
            System.out.println("ket noi that bai. Loi "+ e.getMessage());
        }
    }
      public ResultSet GetData (String jtable) throws SQLException {
          ResultSet kq = null;
          Statement stmt = conn.createStatement();
          String sql = "SELECT * FROM monHoc";
          kq = stmt.executeQuery(sql);
          return kq;
      }  
      
      public boolean insert(String tenMh, String maMh, String cs) {
          boolean check = false;
          connect();
          int idCS = 1;
          for (CoSo coSo : MonHocDAO.coso) {
              if (coSo.getTenCS().equals(cs)) {
                  idCS = coSo.getId_cs();
              }
          }
          String sql = " INSERT INTO monHoc(id_mh,tenMh,diemTB, id_cs) VALUES ( "+ maMh +",'"+ tenMh +"', null, " + idCS + ") ";
          try {
              PreparedStatement pstmt = conn.prepareStatement(sql);
                    
              pstmt.executeUpdate(sql);
//              pstmt.setString(1, tenMh);
                check = true;
          } catch (Exception e) {
              e.printStackTrace();
              check = false;
          }    
        return check;

      }
      
      public boolean delete (int id) {
          boolean check = false;
          connect();
          String sql = " DELETE FROM monHoc WHERE id_mh="+ id +"";
          try {
              PreparedStatement pstmt = conn.prepareStatement(sql);
              pstmt.executeUpdate(sql);
//              pstmt.setString(1, tenMh);
                check = true;
          } catch (Exception e) {
              e.printStackTrace();
              check = false;
          }    
        return check;

      }
      
      public boolean edit (int id,String tenMh, String maMh) {
          boolean check = false;
          connect();
  
          String sql = "UPDATE monHoc SET tenMh = '"+ tenMh +"' WHERE id_mh="+ id +"";
          try {
              PreparedStatement pstmt = conn.prepareStatement(sql);
              pstmt.executeUpdate(sql);
//              pstmt.setString(1, id_mh);
                check = true;
          } catch (Exception e) {
              e.printStackTrace();
              check = false;
          }    
        return check;

      }
      
      public void fillDataJtable(JTable jt) {
          DefaultTableModel model = new DefaultTableModel(new String[]{
              "Mã môn học",     
              "Khối kiến thức",
              "Tên môn học",
              "Điểm trung bình"
          },0);
          try {
             connect();
             stmt = conn.createStatement();
             rs = stmt.executeQuery("SELECT * FROM monHoc");
             while (rs.next()) {
                 String id_mh = rs.getString("id_mh");
                 
                 String tenCS = rs.getString("tenCS");
                 String tenMh = rs.getString("tenMh");
                 String diemTB = rs.getString("diemTB");
                 model.addRow(new Object[]{
                 id_mh,tenMh,diemTB
             });
                 jt.setModel(model);
             }
          } catch (SQLException sqle) {
              System.out.println("Loi o filldata");
          }
      }
      
      public void openFile(String file) {
          try {
              File path = new File(file);
              Desktop.getDesktop().open(path);
          } catch (IOException ioe) {
              System.out.println(ioe);
          }
      }
      
      public void exportExcel(JTable jt) {
          try {
              JFileChooser jFileChooser = new JFileChooser();
              jFileChooser.showSaveDialog(jt);
              File saveFile = jFileChooser.getSelectedFile();
              if (saveFile != null) {
                  saveFile = new File(saveFile.toString()+".xlsx");
                  Workbook wb = new XSSFWorkbook();
                  Sheet sheet = wb.createSheet("danhsach");
                  Row rowCol = sheet.createRow(0);
                  
                  for (int i = 0; i < jt.getColumnCount(); i++) {
                      Cell cell = rowCol.createCell(i);
                      cell.setCellValue(jt.getColumnName(i));
                  }
                  
                  for (int j = 0; j < jt.getRowCount(); j++) {
                      Row row = sheet.createRow(j+1);
                      for ( int k = 0; k < jt.getColumnCount(); k++) {
                          Cell cell = row.createCell(k);
                          if (jt.getValueAt(j,k) != null) {
                              cell.setCellValue(jt.getValueAt(j, k).toString());
                          }
                      }
                  }
                  
                  FileOutputStream out = new FileOutputStream(new File(saveFile.toString()));
                  wb.write(out);
                  wb.close();
                  out.close();
                  openFile(saveFile.toString());
                  JOptionPane.showMessageDialog(null, "Xuất thành công !");
              } else {
//                  JOptionPane.showMessageDialog(null, "");
              }
          } catch (FileNotFoundException e) {
              System.out.println(e);
          } catch (IOException io) {
              System.out.println(io);
          }
      }
      
      public static void loadIdMh() {
          ConnectDB con = new ConnectDB();
          ArrayList <String> listId = new ArrayList<>();
          try {
              Connection conn = con.getConnectDb();
              Statement stsm = conn.createStatement();
              String query = "select id_mh from monHoc";
              ResultSet rs = stsm.executeQuery(query);
              
              while(rs.next()){
                  System.out.println("id: "+ rs.getString(1));
                  listId.add(rs.getString(1));
              }
              
              double diem = 0;
              for (int i = 0; i < listId.size(); i++){
                  diem =  diemTBMon(Integer.parseInt(listId.get(i)));  
                  System.out.println("Diem TB: "+ diem);
                   String queryUpdate = "update monHoc set diemTB = "+ diem + "where id_mh = "+ Integer.parseInt(listId.get(i));
                   stsm.execute(queryUpdate);
              }
          } catch (Exception e) {
              System.out.println("Loi load id mh");
          }
      }
      
      
      public static double diemTBMon(int id_mh) {
          ConnectDB con = new ConnectDB();
          double diemTB = 0;
          ArrayList<String> listDiem = new ArrayList<String>();
          try {
              Connection conn = con.getConnectDb();
              Statement stsm = conn.createStatement();
              String query = "select diem from diem where id_mh = "+ id_mh; 
              ResultSet rs = stsm.executeQuery(query);
              
              while(rs.next()){
                  listDiem.add(rs.getString(1));
              }
              
              double sum = 0;
              for (int i = 0; i < listDiem.size(); i++){
                  sum += Double.parseDouble(listDiem.get(i));
              }
              
              diemTB = sum / listDiem.size();
          } catch (Exception e) {
              System.out.println("Loi tinh diem TB mon");
          }
          
          
          return diemTB;
      }
      
//  
      
      public static void main(String[] args) {
        MonHoc mh = new MonHoc();
        mh.connect();
        
    }

   
}
