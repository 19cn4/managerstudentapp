/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package SubClass;

import java.util.logging.Logger;

/**
 *
 * @author apple
 */
public class Personl {
    private String hoTen;
    private String sex;
    private String address;
    private String birthday;

    public Personl() {
    }

    public Personl(String hoTen, String sex, String address, String birthday) {
        this.hoTen = hoTen;
        this.sex = sex;
        this.address = address;
        this.birthday = birthday;
    }

    public String getHoTen() {
        return hoTen;
    }
    public String getSex() {
        return sex;
    }

    public String getAddress() {
        return address;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

  
    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
   
}
