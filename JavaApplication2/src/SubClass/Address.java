/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package SubClass;

/**
 *
 * @author apple
 */
public class Address {
    private String xa;
    private String huyen;
    private String tinh;

    public Address(String xa, String huyen, String tinh) {
        this.xa = xa;
        this.huyen = huyen;
        this.tinh = tinh;
    }

    public Address() {
        super();
    }

    public String getXa() {
        return xa;
    }

    public String getHuyen() {
        return huyen;
    }

    public String getTinh() {
        return tinh;
    }

    public void setXa(String xa) {
        this.xa = xa;
    }

    public void setHuyen(String huyen) {
        this.huyen = huyen;
    }

    public void setTinh(String tinh) {
        this.tinh = tinh;
    }
    
    
    
    
    
}
