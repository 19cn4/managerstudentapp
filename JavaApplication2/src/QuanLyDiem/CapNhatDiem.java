/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package QuanLyDiem;

/**
 *
 * @author apple
 */
public class CapNhatDiem {
    private int id_mh;
    private String tenMh;

    public int getId_mh() {
        return id_mh;
    }

    public void setId_mh(int id_mh) {
        this.id_mh = id_mh;
    }

    public String getTenMh() {
        return tenMh;
    }

    public void setTenMh(String tenMh) {
        this.tenMh = tenMh;
    }

    public CapNhatDiem(int id_mh, String tenMh) {
        this.id_mh = id_mh;
        this.tenMh = tenMh;
    }
    
}
