/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package QuanLyDiem;


import Login.ConnectDB;
import MenuChinh.MenuChinh;
import static QuanLyMonHoc.MonHoc.conn;
import com.mysql.cj.jdbc.result.ResultSetMetaData;
import java.awt.Color;
import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Stack;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/**
 *
 * @author apple
 */
public class Diem {
    public Connection conn = null;
    public static PreparedStatement pst =null; // biến thực thi sql
    public static ResultSet rs = null; 
    
    public void connect() {
        try {
           conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/manager_student", "nhom6", "Nhom6123456");
           if(conn != null){
                System.out.println("ket noi thanh cong");
           }
        } catch (SQLException e) {
            System.out.println("ket noi that bai. Loi "+ e.getMessage());
        }
    }
    public ResultSet GetData (String jtable) throws SQLException {
          ResultSet kq = null;
          Statement stmt = conn.createStatement();
          String sql = "SELECT sinhVien.msv, sinhVien.hoTen,monHoc.id_mh,monHoc.tenMh,diem.diem FROM manager_student.diem INNER JOIN manager_student.sinhVien on diem.msv = sinhVien.msv inner join manager_student.monHoc ON diem.id_mh = monHoc.id_mh ";
          kq = stmt.executeQuery(sql);
          return kq;
      }  
     public void insert(String msv,JComboBox jcombobox, String diem) {
          connect();
          String sql ="insert into diem(msv, id_mh, diem) values (?,?,?)";
        try
        {
            pst = conn.prepareStatement(sql);
            pst.setString(1, msv);
           
            pst.setString(3, diem);
            for (CapNhatDiem cnd:DiemDAO.cn){
                if (cnd.getTenMh().equals(jcombobox.getSelectedItem())){
                     pst.setInt(2, cnd.getId_mh());
                }
            }
            pst.execute();
            JOptionPane.showMessageDialog(null,"Đã thêm thành công","Thông Báo",1);
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,"Điểm sinh viên ''"+msv+"'' của môn học ''"+jcombobox.getSelectedItem().toString()+"'' đã tồn tại","Thông Báo",1);
        }
      }
      public boolean delete (int msv,JComboBox jcombobox,float diem) {
          String sql = "";
          boolean check = false;
          connect();
          for (CapNhatDiem cnd:DiemDAO.cn){
                if (cnd.getTenMh().equals(jcombobox.getSelectedItem())){
                     sql = " DELETE FROM diem WHERE id_mh="+ cnd.getId_mh()+" and msv ='"+msv+"' and diem ="+diem+"";
                }
            }
          
          try {
              PreparedStatement pstmt = conn.prepareStatement(sql);
              pstmt.executeUpdate(sql);
//              pstmt.setString(1, tenMh);
                check = true;
          } catch (Exception e) {
              e.printStackTrace();
              check = false;
          }    
        return check;

      }
      
      public boolean edit (int msv, JComboBox jcombobox, Float diem) {
           String  sql = "" ;
          boolean check = false;
          connect();
          for (CapNhatDiem cnd:DiemDAO.cn){
                if (cnd.getTenMh().equals(jcombobox.getSelectedItem())){
                     sql = " UPDATE diem SET diem = "+ diem +" WHERE id_mh="+ cnd.getId_mh() +" and msv = "+ msv +" ";
                }
            }
         
          try {
              PreparedStatement pstmt = conn.prepareStatement(sql);
              pstmt.executeUpdate(sql);
//              pstmt.setString(1, id_mh);
                check = true;
          } catch (Exception e) {
              e.printStackTrace();
              check = false;
          }    
        return check;

      }
      public void openFile(String file) {
          try {
              File path = new File(file);
              Desktop.getDesktop().open(path);
          } catch (IOException ioe) {
              System.out.println(ioe);
          }
      }
      public void exportExcel(JTable jt) {
          try {
              JFileChooser jFileChooser = new JFileChooser();
              jFileChooser.showSaveDialog(jt);
              File saveFile = jFileChooser.getSelectedFile();
              if (saveFile != null) {
                  saveFile = new File(saveFile.toString()+".xlsx");
                  Workbook wb = new XSSFWorkbook();
                  Sheet sheet = wb.createSheet("danhsach");
                  Row rowCol = sheet.createRow(0);
                  
                  for (int i = 0; i < jt.getColumnCount(); i++) {
                      Cell cell = rowCol.createCell(i);
                      cell.setCellValue(jt.getColumnName(i));
                  }
                  
                  for (int j = 0; j < jt.getRowCount(); j++) {
                      Row row = sheet.createRow(j+1);
                      for ( int k = 0; k < jt.getColumnCount(); k++) {
                          Cell cell = row.createCell(k);
                          if (jt.getValueAt(j,k) != null) {
                              cell.setCellValue(jt.getValueAt(j, k).toString());
                          }
                      }
                  }
                  
                  FileOutputStream out = new FileOutputStream(new File(saveFile.toString()));
                  wb.write(out);
                  wb.close();
                  out.close();
                  openFile(saveFile.toString());
              } else {
                  JOptionPane.showMessageDialog(null, "Lỗi");
              }
          } catch (FileNotFoundException e) {
              System.out.println(e);
          } catch (IOException io) {
              System.out.println(io);
          }
      }
}
