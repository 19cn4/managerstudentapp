/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package QuanLySinhVien;

import QuanLyMonHoc.MonHoc;
import SubClass.Personl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author apple
 */
public class SinhVien extends Personl{
    
    private int msv;
    private JTable jtable = new JTable();
    private DefaultTableModel tableModel = new DefaultTableModel();
    private Connection cn = null;
    
    public SinhVien() {
        
    }

    public SinhVien(String hoTen, String sex, String address, String birthday) {
        super(hoTen, sex, address, birthday);
    }

    public SinhVien(int msv) {
        this.msv = msv;
    }

    public SinhVien(int msv, String hoTen, String sex, String address, String birthday) {
        super(hoTen, sex, address, birthday);
        this.msv = msv;
    }

    public int getMsv() {
        return msv;
    }

    public void setMsv(int msv) {
        this.msv = msv;
    }
    
    public void connect() {
        try {
           cn = DriverManager.getConnection("jdbc:mysql://localhost:3306/manager_student", "nhom6", "Nhom6123456");
           Statement stmt = cn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM sinhVien");
           if(cn != null){
                System.out.println("ket noi thanh cong");
           }
            
        } catch (SQLException e) {
            System.out.println("ket noi that bai. Loi "+ e.getMessage());
        }
    }
    public ResultSet getData(String jtable) throws SQLException{
          ResultSet kq = null;
          Statement stmt = cn.createStatement();
          String sql = "SELECT * FROM sinhVien";
          kq = stmt.executeQuery(sql);
          return kq;
    }
    
    public static void main(String[] args) {
        SinhVien sv = new SinhVien();
        sv.connect();
    }

    
}
