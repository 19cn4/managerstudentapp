/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package QuanLySinhVien;

import Login.ConnectDB;
import static QuanLyMonHoc.MonHoc.conn;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


/**
 *
 * @author apple
 */
public class CapNhatSinhVien {
    
    private static ArrayList <SinhVien> list = new ArrayList<>();
//    private int autoMsv(){
//    
//    return 
//    }
    
    
    public static void connect(){
        ConnectDB  con = new ConnectDB();
        try {
            Connection conn = con.getConnectDb();
        } catch (Exception e) {
        }
    }
    
  
    
    
    public static void listSV() {
        System.out.println("List SV");
        ConnectDB con = new ConnectDB();
        try {
            Connection conn = con.getConnectDb();
            String query = "select * from sinhVien";
            Statement stsm = conn.createStatement();
            ResultSet rs = stsm.executeQuery(query);
            while(rs.next()){
                int msv = Integer.parseInt(rs.getString(1).trim());
                String gioiTinh  = rs.getString(2);
                String hoTen = rs.getString("hoTen");
                String birthday = rs.getString("birthday");
                String address = rs.getString("address");
                System.out.println("msv: "+ msv);
                System.out.println("msv: "+ address);
               list.add(new SinhVien(msv, hoTen, gioiTinh,birthday, address ));
            }
            
            for (int i = 0; i < list.size(); i++){
                System.out.println("msv: "+ list.get(i).getMsv());
            }
                   
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void them(int msv, String hoTen, String gioiTinh,String birthday , String address, int phoneNumber){
            System.out.println("List SV");
        ConnectDB con = new ConnectDB();
              try {
            Connection conn = con.getConnectDb();
            String query = "insert into sinhVien (msv,hoTen, gioiTinh, birthday, address, phone_number)"
                    + "values ("
                    + msv+"," + "n'"+ hoTen +"',n'" + gioiTinh + "', '" + birthday + "', n'"+ address+"', '" + phoneNumber + "'" 
                    + ")";
            Statement stsm = conn.createStatement();
            stsm.execute(query);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }
    
     public void fillDataJtable(JTable jt) {
          DefaultTableModel model = new DefaultTableModel(new String[]{
              "Mã sinh viên",
              "Họ Tên",
              "Giới tính", 
              "sinh nhật",
              "Địa chỉ",
              "sđt phụ huynh",
              "Điểm TK"
          },0);
          
          ConnectDB con = new ConnectDB();
          try {
             Connection conn = con.getConnectDb();
             Statement stsm = conn.createStatement();
             stsm = conn.createStatement();
            ResultSet rs;
             rs = stsm.executeQuery("SELECT * FROM sinhVien");
             while (rs.next()) {
                 String msv = rs.getString("msv");
                 String hoTen = rs.getString("hoTen");
                 String gioiTinh = rs.getString("gioiTinh");
                 String birthday = rs.getString("birthday");
                 String address = rs.getString("address");
                 double diemTK = Double.parseDouble(rs.getString("diemTK"));
                 int phoneNumber = Integer.parseInt(rs.getString("phone_number"));
                 model.addRow(new Object[]{
                 msv, hoTen, gioiTinh, birthday, address, phoneNumber, diemTK
             });
                 jt.setModel(model);
             }
          } catch (SQLException sqle) {
              System.out.println("Loi xua excel");
          }
      }
     public static void openFile(String file) {
          try {
              File path = new File(file);
              Desktop.getDesktop().open(path);
          } catch (IOException ioe) {
              System.out.println(ioe);
          }
      }
    
    
    public  static void exportExcel(JTable jt) {
          try {
              JFileChooser jFileChooser = new JFileChooser();
              jFileChooser.showSaveDialog(jt);
              File saveFile = jFileChooser.getSelectedFile();
              if (saveFile != null) {
                  saveFile = new File(saveFile.toString()+".xlsx");
                   Workbook wb = new XSSFWorkbook();
                  Sheet sheet = (Sheet) wb.createSheet("Danh sách tất cả sinh viên");
                 Row rowCol = sheet.createRow(0);
                  
                  for (int i = 0; i < jt.getColumnCount(); i++) {
                      Cell cell = rowCol.createCell(i);
                      cell.setCellValue(jt.getColumnName(i));
                  }
                  
                  for (int j = 0; j < jt.getRowCount(); j++) {
                      Row row = sheet.createRow(j+1);
                      for ( int k = 0; k < jt.getColumnCount(); k++) {
                          Cell cell = row.createCell(k);
                          if (jt.getValueAt(j,k) != null) {
                              cell.setCellValue(jt.getValueAt(j, k).toString());
                          }
                      }
                  }
                  
                  FileOutputStream out = new FileOutputStream(new File(saveFile.toString()));
                  wb.write(out);
                  wb.close();
                  out.close();
                  openFile(saveFile.toString());
              } else {
                  JOptionPane.showMessageDialog(null, "Lỗi");
              }
          } catch (FileNotFoundException e) {
              System.out.println(e);
          } catch (IOException io) {
              System.out.println(io);
          }
      }
    
    
    public static void loadDiemTK() {
          ConnectDB con = new ConnectDB();
          ArrayList <String> listId = new ArrayList<>();
          try {
              Connection conn = con.getConnectDb();
              Statement stsm = conn.createStatement();
              String query = "select msv from sinhVien";
              ResultSet rs = stsm.executeQuery(query);
              
              while(rs.next()){
                  System.out.println("id: "+ rs.getString(1));
                  listId.add(rs.getString(1));
              }
              
              double diem = 0;
              for (int i = 0; i < listId.size(); i++){
                  diem =  diemTB(Integer.parseInt(listId.get(i))); 
                  diem = (double) Math.round(diem * 10) / 10;
                  System.out.println("Diem TB: "+ diem);
                   String queryUpdate = "update sinhVien set diemTK = "+ diem + "where msv = "+ Integer.parseInt(listId.get(i));
                   stsm.execute(queryUpdate);
              }
          } catch (Exception e) {
              System.out.println("Loi load msv"+ e.getMessage());
          }
      }
      
      
      public static double diemTB(int msv) {
          ConnectDB con = new ConnectDB();
          double diemTB = 0;
          ArrayList<String> listDiem = new ArrayList<String>();
          try {
              Connection conn = con.getConnectDb();
              Statement stsm = conn.createStatement();
              String query = "select diem from diem where msv = "+ msv; 
              ResultSet rs = stsm.executeQuery(query);
              
              while(rs.next()){
                  listDiem.add(rs.getString(1));
              }
              
              double sum = 0;
              for (int i = 0; i < listDiem.size(); i++){
                  sum += Double.parseDouble(listDiem.get(i));
              }
              
              diemTB = sum / listDiem.size();
          } catch (Exception e) {
              System.out.println("Loi tinh diem TB ");
          }
          
          
          return diemTB;
      }
      
    
 
    

    
    
    
}
