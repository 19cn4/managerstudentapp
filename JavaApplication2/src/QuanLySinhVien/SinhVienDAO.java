/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package QuanLySinhVien;

import MenuChinh.MenuChinh;
import static QuanLySinhVien.CapNhatSinhVien.listSV;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import Login.ConnectDB;
import QuanLyMonHoc.MonHoc;
import static QuanLyMonHoc.MonHocDAO.rs;
import static QuanLySinhVien.CapNhatSinhVien.loadDiemTK;
import static QuanLySinhVien.CapNhatSinhVien.them;
import javax.swing.JOptionPane;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

/**
 *
 * @author apple
 */
public class SinhVienDAO extends javax.swing.JFrame {

    final String header[] = {"MSV", "Họ tên", "Giới tính", "Ngày sinh", "Quê quán"};
    final DefaultTableModel tb = new DefaultTableModel(header, 0);
    ConnectDB cndb = new ConnectDB();
    Connection cn;
    public static ResultSet rs = null;
    public Statement stmt;

    int controller = 0;

    /**
     * Creates new form SinhVienDAO
     */
    public SinhVienDAO() {
        initComponents();
        setMsv();
        SinhVien sv = new SinhVien();
        sv.connect();
        loadDiemTK();
        loadData();
//        frTrue.hide();
//        lbNew.hide();
//        txtNew.hide();
//        btnSaveNew.hide();
//        txtNewDay.hide();
//        txtNewMonth.hide();
//        txtNewYear.hide();
//        lbNewDay.hide();
//        lbNewMonth.hide();
//        lbNewYear.hide();
    }

    public void loadData() {
        SinhVien sv = new SinhVien();
        sv.connect();
        try {
            rs = sv.getData("sinhVien");
            jTable1.removeAll();
            String[] arr = {"MSSV", "Họ và tên", "Giới tính", "Ngày sinh", "Địa chỉ", "SĐT phụ huynh", "Điểm TK"};
            DefaultTableModel model = new DefaultTableModel(arr, 0);

            while (rs.next()) {
                Vector vec = new Vector();
                vec.add(rs.getString("msv"));
                vec.add(rs.getString("hoTen"));
                vec.add(rs.getString("gioiTinh"));
                vec.add(rs.getString("birthday"));
                vec.add(rs.getString("address"));
                vec.add(rs.getString("phone_number"));
                vec.add(rs.getString("diemTK"));
                model.addRow(vec);
            }
            jTable1.setModel(model);
//            TableColumnModel tcm = jTable1.getColumnModel();
//            tcm.removeColumn(tcm.getColumn(0));

        } catch (Exception e) {
            System.out.println("Loi load data");
        }

    }

    private void setMsv() {
        ConnectDB con = new ConnectDB();

        try {
            Connection conn = con.getConnectDb();
            String queryMsv = "select max(msv) from sinhVien";
            Statement stsm = conn.createStatement();
            ResultSet rs = stsm.executeQuery(queryMsv);
            int msv = 0;
           while (rs.next()) {
                msv = Integer.parseInt(rs.getString(1));
                msv+=1;
                txtMsv.setText("" + (msv) + "");
            } 
        } catch (Exception e) {
            txtMsv.setText("100");
        }

    }
//    Load Bảng
    private void setFilter() {
        ConnectDB con = new ConnectDB();

        try {
            
            Connection conn = con.getConnectDb();
            Statement stsm = conn.createStatement();
            String query = "select * from sinhVien";
            
            ResultSet rs = stsm.executeQuery(query);
//            LoadTable(query, header);
            try {
                
                jTable1.removeAll();
                String[] arr = { "Họ và tên", "Giới tính", "Ngày sinh", "Địa chỉ", "SĐT phụ huynh", "Điểm TK"};
                DefaultTableModel model = new DefaultTableModel(arr, 0);

                while (rs.next()) {
                    Vector vec = new Vector();
//                    vec.add(rs.getString("msv"));
                    vec.add(rs.getString("hoTen"));
                    vec.add(rs.getString("gioiTinh"));
                    vec.add(rs.getString("birthday"));
                    vec.add(rs.getString("address"));
                    vec.add(rs.getString("phone_number"));
                    vec.add(rs.getString("diemTK"));
                    model.addRow(vec);
                }
                jTable1.setModel(model);
//            TableColumnModel tcm = jTable1.getColumnModel();
//            tcm.removeColumn(tcm.getColumn(0));

            } catch (Exception e) {
                System.out.println("Loi setFilter");
            }

//            if (rs.next()){
//                JOptionPane.showMessageDialog(this, "chọn thông tin cần sửa");
////                frTrue.show();
//                
//            }else {
//                JOptionPane.showMessageDialog(this, "Sinh viên không có trong hệ thống");
//            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }

    }
    
    
        private void setFilterMsv() {
        ConnectDB con = new ConnectDB();

        try {
            Connection conn = con.getConnectDb();
            Statement stsm = conn.createStatement();
            String msvFind = txtFindMsv.getText().trim();
            String query = "select * from sinhVien where msv like '%"+msvFind+"%' or hoTen like '%" + msvFind + "%'";
            ResultSet rs = stsm.executeQuery(query);
//            LoadTable(query, header);
            try {
                
                jTable1.removeAll();
                String[] arr = {"MSSV", "Họ và tên", "Giới tính", "Ngày sinh", "Địa chỉ","SĐT phụ huynh", "Điểm TK"};
                DefaultTableModel model = new DefaultTableModel(arr, 0);

                while (rs.next()) {
                    Vector vec = new Vector();
                    vec.add(rs.getString("msv"));
                    vec.add(rs.getString("hoTen"));
                    vec.add(rs.getString("gioiTinh"));
                    vec.add(rs.getString("birthday"));
                    vec.add(rs.getString("address"));
                    vec.add(rs.getString("phone_number"));
                    vec.add(rs.getString("diemTK"));
                    model.addRow(vec);
                }
                jTable1.setModel(model);
//            TableColumnModel tcm = jTable1.getColumnModel();
//            tcm.removeColumn(tcm.getColumn(0));

            } catch (Exception e) {
                System.out.println("Loi setFilters");
            }

//            if (rs.next()){
//                JOptionPane.showMessageDialog(this, "chọn thông tin cần sửa");
////                frTrue.show();
//                
//            }else {
//                JOptionPane.showMessageDialog(this, "Sinh viên không có trong hệ thống");
//            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        kGradientPanel1 = new keeptoo.KGradientPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtMsv = new javax.swing.JTextField();
        txtHoten = new javax.swing.JTextField();
        txtGioiTinh = new javax.swing.JTextField();
        txtNamSinh = new javax.swing.JTextField();
        txtThang = new javax.swing.JTextField();
        txtDay = new javax.swing.JTextField();
        txtQue = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtPhone = new javax.swing.JTextField();
        txtFindMsv = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        backBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Họ Và tên", "Giới tính", "Ngày sinh", "Quê quán", "SĐT Phụ Huynh", "Điểm TK"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        kGradientPanel1.setkEndColor(new java.awt.Color(0, 255, 204));
        kGradientPanel1.setkStartColor(new java.awt.Color(0, 204, 255));

        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/btn/plus_+_35px.png"))); // NOI18N
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        jPanel4.add(btnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 310, -1, -1));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel2.setText("Mã sinh viên");
        jPanel4.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 80, 120, -1));

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel6.setText("Họ và tên");
        jPanel4.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 110, 100, 30));

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel7.setText("Giới tính");
        jPanel4.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 160, 70, 20));

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel8.setText("Quê quán");
        jPanel4.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 230, 80, -1));

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel9.setText("Ngày sinh(dd/mm/yyyy)");
        jPanel4.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 190, -1, -1));

        txtMsv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMsvActionPerformed(evt);
            }
        });
        jPanel4.add(txtMsv, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 70, 280, 30));
        jPanel4.add(txtHoten, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 110, 280, 30));
        jPanel4.add(txtGioiTinh, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 150, 70, 30));
        jPanel4.add(txtNamSinh, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 190, 70, 30));
        jPanel4.add(txtThang, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 190, 90, 30));
        jPanel4.add(txtDay, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 190, 80, 30));
        jPanel4.add(txtQue, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 230, 280, 30));
        jPanel4.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 10, 140, -1));

        jLabel12.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel12.setText("SĐT phụ huynh");
        jPanel4.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 270, 100, -1));
        jPanel4.add(txtPhone, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 270, 280, 30));

        txtFindMsv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFindMsvActionPerformed(evt);
            }
        });
        jPanel4.add(txtFindMsv, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 20, 280, 30));

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/btn/search.png"))); // NOI18N
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 10, 60, -1));

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/btn/edit-button.png"))); // NOI18N
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 310, -1, -1));

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/btn/trash.png"))); // NOI18N
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 310, -1, -1));

        jLabel11.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel11.setText("Tìm kiếm (mssv/họ tên)");
        jPanel4.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 30, -1, -1));

        jTabbedPane1.addTab("Thêm sinh viên", jPanel4);

        backBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/btn/left-arrow.png"))); // NOI18N
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("QUẢN LÝ SINH VIÊN");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/btn/microsoft_excel_35px.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout kGradientPanel1Layout = new javax.swing.GroupLayout(kGradientPanel1);
        kGradientPanel1.setLayout(kGradientPanel1Layout);
        kGradientPanel1Layout.setHorizontalGroup(
            kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(kGradientPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
            .addGroup(kGradientPanel1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(backBtn)
                .addGap(202, 202, 202)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(14, 14, 14))
        );
        kGradientPanel1Layout.setVerticalGroup(
            kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, kGradientPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(backBtn, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(63, 63, 63))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 753, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(kGradientPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(105, 105, 105))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(kGradientPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        // TODO add your handling code here:
        this.hide();
        MenuChinh menu = new MenuChinh();
        menu.show();
    }//GEN-LAST:event_backBtnActionPerformed

    private void txtMsvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMsvActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMsvActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        // TODO add your handling code here:
        //        listSV();

        ConnectDB con = new ConnectDB();

        try {
            Connection conn = con.getConnectDb();
            if (txtMsv.getText().length() == 0 || txtHoten.getText().length() == 0 || txtGioiTinh.getText().length() == 0
                || txtNamSinh.getText().length() == 0 || txtThang.getText().length() == 0 || txtDay.getText().length() == 0
                || txtQue.getText().length() == 0 || txtPhone.getText().length() == 0) {
                JOptionPane.showMessageDialog(this, "Không đươc để trống thông tin nào");
            } else {

                String queryMsv = "select max(msv) from sinhVien";
                Statement stsm = conn.createStatement();
                ResultSet rs = stsm.executeQuery(queryMsv);
                int msv = 0;
                try {
                    while (rs.next()) {
                    msv = Integer.parseInt(rs.getString(1));
                    msv+=1;
                    txtMsv.setText("" + msv + "");
                }
                } catch (Exception e) {
                     txtMsv.setText("100");
                     msv = 100;
                }
               

                String hoTen = txtHoten.getText();

                String gioiTinh = "";
                if (txtGioiTinh.getText().trim().equalsIgnoreCase("nam") == true || txtGioiTinh.getText().equalsIgnoreCase("nữ") == true) {
                    gioiTinh = txtGioiTinh.getText();
                    System.out.println(txtGioiTinh.getText());
                } else {
                    JOptionPane.showMessageDialog(this, "Giới tính nhập sai");
                }

                String birthday = "";
                int nam = Integer.parseInt(txtNamSinh.getText().trim());
                int thang = Integer.parseInt(txtThang.getText().trim());
                int ngay = Integer.parseInt(txtDay.getText().trim());
                
            
                if (nam <= 1990 || thang < 1 || thang > 12 || ngay < 1 || ngay > 31) {
                    JOptionPane.showMessageDialog(this, "Thông tin ngày sinh sai");
                } else {

                    birthday = nam + "-" + thang + "-" + ngay;
                }
                String address = txtQue.getText();
                int phoneNumber = Integer.parseInt(txtPhone.getText());
                
                System.out.println();
                if (txtPhone.getText().length() < 10){
                    JOptionPane.showMessageDialog(this, "Sai so dien thoai");
                }
                
                msv = Integer.parseInt(txtMsv.getText());
               
                them(msv, hoTen, gioiTinh, birthday, address, phoneNumber);
                JOptionPane.showMessageDialog(this, "Thêm sinh viên thành công");
                
                setFilter();
                setMsv();
                txtHoten.setText("");
                txtGioiTinh.setText("");
                txtNamSinh.setText("");
                txtThang.setText("");
                txtDay.setText("");
                txtQue.setText("");
                txtPhone.setText("");
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "xuất hiện lỗi: " + e.getMessage());
            System.out.println(e.getMessage());

        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
       
         setFilterMsv();
//        ConnectDB con = new ConnectDB();
//        try {
//            Connection conn = con.getConnectDb();
//            String query = "select * from sinhVien where id = "+ txtFindMsv;
//                Statement stsm = conn.createStatement();
//                ResultSet rs = stsm.executeQuery(query);
////                int msv = Integer.parseInt(txtFindMsv.getText().trim());
//                if (rs.next()){
//                    setFilterMsv();
//                }else {
//                JOptionPane.showMessageDialog(this, "Sinh viên không có trong danh sách");
//    
//                }
//        } catch (Exception e) {
//        }
        
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        
        ConnectDB con = new ConnectDB();
        
        try {
            Connection conn = con.getConnectDb();
            if (txtMsv.getText().length() == 0 || txtHoten.getText().length() == 0 || txtGioiTinh.getText().length() == 0
                || txtNamSinh.getText().length() == 0 || txtThang.getText().length() == 0 || txtDay.getText().length() == 0
                || txtQue.getText().length() == 0 || txtPhone.getText().length() == 0) {
                JOptionPane.showMessageDialog(this, "Không đươc để trống thông tin nào");
            } else {
                String hoTen = txtHoten.getText();

                String gioiTinh = "";
                if (txtGioiTinh.getText().trim().equalsIgnoreCase("nam") == true ||
                        txtGioiTinh.getText().equalsIgnoreCase("nữ") == true) {
                    gioiTinh = txtGioiTinh.getText();
                    String birthday = "";
                int nam = Integer.parseInt(txtNamSinh.getText().trim());
                int thang = Integer.parseInt(txtThang.getText().trim());
                int ngay = Integer.parseInt(txtDay.getText().trim());

                if (nam <= 1990 || thang < 1 || thang > 12 || ngay < 1 || ngay > 31) {
                    JOptionPane.showMessageDialog(this, "Thông tin ngày sinh sai");
                } else {

                    birthday = nam + "-" + thang + "-" + ngay;
                     String address = txtQue.getText();
                    int phoneNumber = Integer.parseInt(txtPhone.getText());
                
                
                if (txtPhone.getText().length() < 10){
                    JOptionPane.showMessageDialog(this, "Sai so dien thoai");
                }else {
                
                 String query = " update sinhVien \n" +
                " set hoTen =  n'"+txtHoten.getText().trim()+"',"
                        + " gioiTinh = n'"+txtGioiTinh.getText().trim()+"',"
                        + " birthday = '"+birthday+"',"
                        + " address = n'"+txtQue.getText().trim()+"', "
                        + "phone_number = "+Integer.parseInt(txtPhone.getText().trim())+"\n" +
                    " where msv = " + Integer.parseInt(txtMsv.getText().trim());
                
                Statement stsm = conn.createStatement();
                    stsm.execute(query);
        
                
                JOptionPane.showMessageDialog(this, "Thêm sinh viên thành công");
            
                setFilter();
                txtMsv.setText("");
                txtHoten.setText("");
                txtGioiTinh.setText("");
                txtNamSinh.setText("");
                txtThang.setText("");
                txtDay.setText("");
                txtQue.setText("");
                txtPhone.setText("");
                
                }
                
               
                }
               
                } else {
                    JOptionPane.showMessageDialog(this, "Giới tính nhập sai");
                }

                
            }
            }catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Loi");
                }
        
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
          int i = jTable1.getSelectedRow();
        TableModel model = jTable1.getModel();
        txtMsv.setText(model.getValueAt(i,0).toString());
        txtHoten.setText(model.getValueAt(i,1).toString());
        txtGioiTinh.setText(model.getValueAt(i,2).toString());
        String[] birthday = new String[3];
        birthday = (model.getValueAt(i,3).toString()).split("-");
        txtDay.setText(String.valueOf(birthday[2]));
        txtThang.setText(String.valueOf(birthday[1]));
        txtNamSinh.setText(String.valueOf(birthday[0]));
        txtQue.setText(model.getValueAt(i,4).toString());
        txtPhone.setText(model.getValueAt(i,5).toString());
        
    }//GEN-LAST:event_jTable1MouseClicked

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        
        ConnectDB con = new ConnectDB();
        
        try {
            Connection conn = con.getConnectDb();
            Statement stsm = conn.createStatement();
            String query = "delete from sinhVien where msv = " + txtMsv.getText().trim();
            stsm.execute(query);
            JOptionPane.showMessageDialog(this, "Xoá sinh viên thành công");
            setFilter();
              setMsv();
                txtHoten.setText("");
                txtGioiTinh.setText("");
                txtNamSinh.setText("");
                txtThang.setText("");
                txtDay.setText("");
                txtQue.setText("");
                txtPhone.setText("");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Xoá sinh viên lỗi "+ e.getMessage());
        }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try {
            CapNhatSinhVien.exportExcel(jTable1);
        } catch (Exception e) {
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtFindMsvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFindMsvActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFindMsvActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SinhVienDAO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SinhVienDAO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SinhVienDAO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SinhVienDAO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SinhVienDAO().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backBtn;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private keeptoo.KGradientPanel kGradientPanel1;
    private javax.swing.JTextField txtDay;
    private javax.swing.JTextField txtFindMsv;
    private javax.swing.JTextField txtGioiTinh;
    private javax.swing.JTextField txtHoten;
    private javax.swing.JTextField txtMsv;
    private javax.swing.JTextField txtNamSinh;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JTextField txtQue;
    private javax.swing.JTextField txtThang;
    // End of variables declaration//GEN-END:variables
}
